class Photo < ApplicationRecord
  has_many_attached :images
  
  belongs_to :user
  belongs_to :place

  validates :user_id, :place_id, presence: true

  def self.to_tree
    tree = {}
    Photo.all.each do |p|
      tree = tree.deeper_merge!({
        p.date.strftime('%Y') => {
          "#{p.place.name}key_separator#{p.place.id}" => {
            p.date.strftime('%B') => {
              p.date.strftime('%d') => p.images.map{|i|
                {
                  url: Rails.application.routes.url_helpers.url_for(i),
                  filename: i.filename.to_s,
                  id: i.id
                }
              }
            }
          }
        }
      })
    end
    return tree
  end

  def self.process_file(image)
    file_path = "#{Dir.tmpdir}/#{SecureRandom.urlsafe_base64}"
    File.open(file_path, 'wb') do |file|
      file.write(image.download)
    end   

    file_path
  end

  def self.photos_filter(year = nil, place = nil, month = nil, day = nil)
    files = [] 

    if day
      date = Date.strptime("#{day} #{month} #{year}", "%d %B %Y")
      photos = Photo.where('date >= ?', date.beginning_of_day).where('date <= ?', date.end_of_day).where(place_id: place)
    elsif month
      date = Date.strptime("#{month} #{year}", "%B %Y")
      photos = Photo.where('date >= ?', date.beginning_of_month).where('date <= ?', date.beginning_of_month).where(place_id: place)
    elsif place
      date = Date.strptime("#{year}", "%Y")
      photos = Photo.where('date >= ?', date.beginning_of_year).where('date <= ?', date.end_of_year).where(place_id: place)
    elsif year
      date = Date.strptime("#{year}", "%Y")
      photos = Photo.where('date >= ?', date.beginning_of_year).where('date <= ?', date.end_of_year)
    else
      photos = Photo.all
    end
    photos.each{ |p| p.images.each{ |image| files << [image, "#{p.date.strftime('%Y')}/#{p.place.name}/#{p.date.strftime('%B')}/#{p.date.strftime('%d')}/#{image.filename.to_s}"] } }
    
    files
  end
end
