class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :registerable, :recoverable
	devise :database_authenticatable, :rememberable, :trackable, :recoverable

  EMAIL_REGEXP = /\A(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})\z/i
	ROLES = %i[user admin]

  belongs_to :current_place, class_name: 'Place'
  has_many :orders
  has_many :photos

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: EMAIL_REGEXP }
  validates :password, length: { within: 6..18 }, unless: :skip_password?

  after_commit :send_info_letter, on: :create

  def today_current_place
    if current_place && current_place_set_at >= (Time.now.hour >= 8 ? Time.now.change(hour: 8) : 1.day.ago.change(hour: 8))
      current_place
    else
      nil
    end
  end

  def role?(role_name)
    role == role_name
  end

  def skip_password?
    @skip_password ||= false
  end

  def update_without_password(params, *options)
    instance_variable_set(:@skip_password, true)
    result = super
    instance_variable_set(:@skip_password, false)
    result
  end

  private

  def send_info_letter
    UserInfoMailer.send_user_info(self.id, self.password).deliver_later
  end
	
end
