class Place < ApplicationRecord
  has_many :orders

  validates :name, :currency, presence: true
  validates_numericality_of :apple_on_fresh_juice, :apple_on_milk, :apple_on_water,
   :grapefruit_on_fresh_juice, :grapefruit_on_milk, :grapefruit_on_water, :melon_on_fresh_juice, :melon_on_milk, 
   :melon_on_water, :pineapple_on_fresh_juice, :pineapple_on_milk, :pineapple_on_water, :watermelon_hookan, greater_than: 0


  def earnings(from, to)
    Order.where(place_id: self.id).where('date >= ?', from.beginning_of_month.beginning_of_day).where('date <= ?', to.end_of_month.end_of_day).sum(:total)
  end
end
