class Order < ApplicationRecord
  before_save :set_price

  Based = ['on_water','on_fresh_juice','on_milk']
  Aqua = ['apple','grapefruit','melon','pineapple','watermelon_hookan']

  belongs_to :place
  belongs_to :user

  validates :based, :aqua, :place, :user, :date, presence: true
  
  attr_accessor :date_from, :date_to

  def set_price
    if (aqua == 'watermelon_hookan')
      check = 'watermelon_hookan'
    else 
      check = aqua + "_" + based
    end

    self.currency = place.currency
    self.total = place[check]
  end

  def self.month_days(month,year=Date.today.year)
    mdays = [nil,31,28,31,30,31,30,31,31,30,31,30,31]
    mdays[2] = 29 if Date.leap?(year)
    mdays[month]
  end

  def self.create_report(order_params,params)
    @start_date = Time.mktime(Date.today.year,params[:month],01,00,00,0)
    @end_day = month_days(params[:month].to_i)
    @end_date = Time.mktime(Date.today.year,params[:month],@end_day,23,59,59)
    @orderlist = Order.where("place ==? AND date > ? AND date < ?",order_params[:place],@start_date,@end_date)
    @calc = {}

    Aqua.each do |aqua|
      Based.each do |based|
        @i = 1
        @calc["#{aqua}_#{based}"] = {}
        while @i <= @end_day do
          @calc["#{aqua}_#{based}"]["#{@i}"] = 0
          @i += 1
        end
      end
    end

    @orderlist.each do |child|
        @calc["#{child[:aqua]}_#{child[:based]}"]["#{child[:date].day}"] += 1
    end
    @calc
  end

  def self.generate_report(period_start, period_end, place_id)
    variants = []
    Order::Aqua.each{|a| Order::Based.each{|b| variants<<[a,b]}}
    periods = (period_start.to_date..period_end.to_date).group_by{|d| "#{Date::MONTHNAMES[d.month]} #{d.year}"}.map { |k,v| [k, v.first.beginning_of_month, v.first.end_of_month] }
    periods_data = []
    periods.each do |p|
      from = p.second
      to = p.last
      counts = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("(date_trunc('day', date), orders.based, orders.aqua)").count
      totals = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("(orders.based, orders.aqua)").count
      day_total = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("date_trunc('day', date)").count
      total_profits = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("(orders.based, orders.aqua)").sum(:total)
      sum_total = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("orders.aqua").count
      sum_total_profits = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).group("orders.aqua").sum(:total)
      watermelon_hookan_counts = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).where(aqua: 'watermelon_hookan').group("date_trunc('day', date)").count
      watermelon_hookan_total = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).where(aqua: 'watermelon_hookan').count
      watermelon_hookan_profits = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).where(aqua: 'watermelon_hookan').sum(:total)
      total_count = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).count
      total_profit = Order.where(place_id: place_id).where('date >= ?', from.beginning_of_day).where('date <= ?', to.end_of_day).sum(:total)

      periods_data << {
        period_name: p.first,
        period_start: from,
        period_end: to,
        counts: counts,
        totals: totals,
        day_total: day_total,
        total_profits: total_profits,
        sum_total: sum_total,
        sum_total_profits: sum_total_profits,
        variants: variants,
        watermelon_hookah: {
          counts: watermelon_hookan_counts,
          total: watermelon_hookan_total,
          profits: watermelon_hookan_profits
        },
        total_count: total_count,
        total_profit: total_profit
      }
    end
    periods_data
  end

end
