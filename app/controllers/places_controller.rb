class PlacesController < ApplicationController
	before_action :authenticate_user!
  before_action :check_admin

	def index
    @places = Place.all
  end

	def show
    @place = Place.find(params[:id])
  end

  def new
    @place = Place.new
  end

  def edit
  	@place = Place.find(params[:id])
	end

  def create
  	@place = Place.new(save_params)
 
  	if @place.save
  	 redirect_to places_path, notice: 'Place created'
    else
      render :new
    end
  end

  def update
  	@place = Place.find(params[:id])
 
  	if @place.update(save_params)
    	redirect_to places_path, notice: 'Place updated'
  	else
    	render 'edit'
  	end
	end

	def destroy
  	@place = Place.find(params[:id])
  	@place.destroy
 
  	redirect_to places_path
	end

  private
  def save_params
    params.require(:place).permit(:name, :currency,:apple_on_fresh_juice,:apple_on_milk,:apple_on_water,:grapefruit_on_fresh_juice,:grapefruit_on_milk,:grapefruit_on_water,:melon_on_fresh_juice,:melon_on_milk,:melon_on_water,:pineapple_on_fresh_juice,:pineapple_on_milk,:pineapple_on_water,:watermelon_hookan)
  end

end
