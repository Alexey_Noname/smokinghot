class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters  
    default_keys = %i(role email password password_confirmation)
    devise_parameter_sanitizer.permit(:sign_up, keys: default_keys)
    devise_parameter_sanitizer.permit(:account_update, keys: default_keys)
  end
    
  def check_admin
    redirect_to orders_path if current_user && !current_user.role?('admin')
  end
end
