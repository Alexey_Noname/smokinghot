class UsersController < ApplicationController
	before_action :authenticate_user!
  before_action :check_admin, except: :set_todays_place
  before_action :find_user, only: [:show, :edit, :update, :destroy]
	before_action

	def index
    @users = User.all
  end

	def show
  end

  def new
    @user = User.new
  end

  def edit
	end

  def create
  	@user = User.new(save_params)
 
  	if @user.save
  	 redirect_to @user
    else
      render :new
    end
  end

  def update
  	if @user.update_without_password(save_params)
    	redirect_to @user
  	else
    	render 'edit'
  	end
	end

	def destroy
  	@user.destroy
  	redirect_to users_path
	end

  def set_todays_place
    place = Place.find_by_name(params[:place_name])
    User.where(id: current_user.id).update_all(current_place_id: place.id, current_place_set_at: Time.now) if place
  end


  private

  def find_user
    @user = User.find(params[:id])
  end
  def save_params
    params.require(:user).permit(:email, :role, :password, :password_confirmation)
  end

end
