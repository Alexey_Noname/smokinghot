class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :check_current_place, only: :create
  before_action

  def index
    @from = Date.strptime(params[:order][:date_from], "%d.%m.%Y") rescue 1.day.ago.beginning_of_day
    @to = Date.strptime(params[:order][:date_to], "%d.%m.%Y").end_of_day rescue Date.today.end_of_day
    
    @orders = (current_user.role?('admin') ? Order.all : current_user.orders).where(created_at: @from..@to).order('created_at DESC').paginate(page: params[:page], per_page: 10)
    @order = Order.new
  end

  def show
    @order = Order.find(params[:id])
  end

  def edit
    @order = Order.find(params[:id])
    authorize! :update, @order
  end

  def create
    @order = current_user.orders.new(save_params.merge(place: current_user.today_current_place, date: Time.now))
    authorize! :create, @order
    if @order.save
      redirect_to orders_path, notice: 'Order created'
    else
      redirect_to orders_path, alert: 'Order was not created'
    end
  end

  def update
    @order = Order.find(params[:id])
    authorize! :update, @order
    @order.date = Time.now
    if @order.update(save_params)
      redirect_to orders_path, notice: 'Order updated'
    else
      render 'edit'
    end
  end

  def destroy
    @order = Order.find(params[:id])
    authorize! :destroy, @order
    @order.destroy
    redirect_to orders_path
  end


  def getreport
    @from = Date.strptime(params[:order][:date_from], "%m-%Y") rescue Date.today.beginning_of_month
    @to = Date.strptime(params[:order][:date_to], "%m-%Y") rescue Date.today
    @place = Place.find(params[:order][:place_id]) rescue Place.first
    @report = Order.generate_report(@from, @to, @place.id)
    @total = @place.earnings(@from, @to)
    respond_to do |format|
      format.html
      format.xls
    end

  end


  private

  def check_current_place
    unless current_user.today_current_place
      flash[:alert] = "Chose place for today first"
      redirect_to orders_path, alert: "Chose place for today first"
    end
  end

  def save_params
    params.require(:order).permit(:based, :aqua, :place_id)
  end
end
