class PhotosController < ApplicationController
	before_action :authenticate_user!
  before_action :check_admin, only: :download

  require 'zip'

	def index
    if current_user.role? 'admin'
      @photos = Photo.to_tree 
    else
      redirect_to new_photo_path
    end
  end

  def new
    @photo = Photo.new(user: current_user, place_id: current_user.today_current_place.try(:id), date: Date.today.strftime('%d.%m.%Y'))
  end

  def create
    @photo = Photo.new(save_params.merge({user_id: current_user.id}))
    authorize! :create, @photo
    if @photo.save && params[:photo][:images]
      @photo.images.attach(params[:photo][:images])
      redirect_to photos_path, notice: 'Upload success'
    else
      @photo.errors.add(:images, 'No images selected') unless params[:photo][:images]
      render :new
    end
  end

  def update
    create
  end

  def download
    files = Photo.photos_filter(params[:year], params[:place], params[:month], params[:day])

    respond_to do |format|
      format.html
      format.zip do
        compressed_filestream = Zip::OutputStream.write_buffer do |zos|
          files.each do |file|
            zos.put_next_entry file.last
            zos.print file.first.download
          end
        end
        compressed_filestream.rewind
        send_data compressed_filestream.read, filename: "images.zip"
      end
    end
  end

  def delete
    @file = ActiveStorage::Attachment.find(params[:id])
    authorize! :delete, @file.record
    @file.record.delete if @file.delete && (@file.record.images.count == 0)
    respond_to do |format|
      format.html { redirect_to new_photo_path}
      format.js
    end
  end

  private

  def save_params
    params.require(:photo).permit(:user, :place_id, :date)
  end

end
