class UserInfoMailer < ApplicationMailer
  def send_user_info(id, password)
    @user = User.find(id)
    return unless @user
    @password = password
    mail(to: @user.email, subject: 'Your account info')
  end
end
