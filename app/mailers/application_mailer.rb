class ApplicationMailer < ActionMailer::Base
  default from: 'Shishas <enjoykaz@gmail.com>'
  layout 'mailer'
end
