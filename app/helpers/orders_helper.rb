module OrdersHelper

	def options_for_based
  		[
    			['On water','on_water'],
    			['On fresh juice','on_fresh_juice'],
    			['On milk','on_milk']
  		]
	end

	def options_for_aqua
  		[
    			['On apple','apple'],
    			['On Grapefruit','grapefruit'],
    			['On Melon','melon'],
    			['On Pineaple','pineapple'],
    			['Watermelon Hookah','watermelon_hookan']
  		]
	end

  def options_for_month
      [
          ['On apple','0'],
          ['On Grapefruit','1'],
          ['On Melon','2'],
          ['On Pineaple','3'],
          ['Watermelon Hookah','watermelon_hookan']
      ]
  end
end
