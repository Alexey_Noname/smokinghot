$(document).ready ->
  $('.datepicker-date').datepicker
    format: 'dd.mm.yyyy'

  $('.spoiler-trigger').click ->
    $(this).parent().next().collapse 'toggle'