# config valid for current version and patch releases of Capistrano
lock "3.11.0"

set :application, "shishas"
set :repo_url, "git@bitbucket.org:Alexey_Noname/smokinghot.git"
set :branch, 'master'
set :user,            'deploy'
set :puma_threads,    [4, 16]
set :puma_workers,    0

set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
# set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true 

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/puma.rb', 'config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads', 'storage')

set :puma_conf, "#{shared_path}/config/puma.rb"

set :rvm_ruby_version, '2.4.1'


# set :nginx_server_name_to_redirect, 'demmo.net www.demmo.net'
# set :nginx_server_name, 'demmo.net'
# set :nginx_use_ssl, true
# set :nginx_ssl_certificate, '/etc/letsencrypt/live/demmo.net/fullchain.pem'
# set :nginx_ssl_certificate_key, '/etc/letsencrypt/live/demmo.net/privkey.pem'

namespace :deploy do
  before 'check:linked_files', 'puma:config'
  before 'check:linked_files', 'puma:nginx_config'
  after 'puma:smart_restart', 'nginx:restart'
end