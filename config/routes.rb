Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'orders/index'

  match 'orders/getreport' => 'orders#getreport', via: [:get, :post]

  get 'orders/index', as: 'user_root'
 
  resources :orders
  resources :places
  resources :photos do
    get :download, on: :collection
    delete :delete, on: :member
  end
	resources :users, path: 'staff'

  post 'set_todays_place' => 'users#set_todays_place'

  root 'orders#index'
end
