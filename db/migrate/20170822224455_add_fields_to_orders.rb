class AddFieldsToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :base_cost, :integer
    add_column :orders, :general_cost, :integer
    add_column :orders, :currency, :string
  end
end
