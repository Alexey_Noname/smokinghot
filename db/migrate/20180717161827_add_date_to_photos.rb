class AddDateToPhotos < ActiveRecord::Migration[5.2]
  def change
    add_column :photos, :date, :datetime
  end
end
