class AddCurrentPlaceToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :current_place_id, :integer
    add_column :users, :current_place_set_at, :datetime
  end
end
