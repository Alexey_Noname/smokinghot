class AddFieldsToPlaces < ActiveRecord::Migration[5.0]
  def change
    add_column :places, :apple_on_fresh_juice, :integer
    add_column :places, :apple_on_milk, :integer
    add_column :places, :apple_on_water, :integer
    add_column :places, :grapefruit_on_fresh_juice, :integer
    add_column :places, :grapefruit_on_milk, :integer
    add_column :places, :grapefruit_on_water, :integer
    add_column :places, :melon_on_fresh_juice, :integer
    add_column :places, :melon_on_milk, :integer
    add_column :places, :melon_on_water, :integer
    add_column :places, :pineapple_on_fresh_juice, :integer
    add_column :places, :pineapple_on_milk, :integer
    add_column :places, :pineapple_on_water, :integer
    add_column :places, :watermelon_hookan, :integer
	end
end
