class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.datetime :date
      t.string :place
      t.string :based
      t.string :aqua
      t.integer :total

      t.timestamps
    end
  end
end
