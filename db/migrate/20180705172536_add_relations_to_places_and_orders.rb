class AddRelationsToPlacesAndOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :place_id, :integer
    remove_column :orders, :place
  end
end
